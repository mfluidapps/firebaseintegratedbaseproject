import axios from 'axios';
import {navigate} from '../../navigation/navigationRef';
import {AsyncStorage} from 'react-native';
var my_acesscode = '';
const localURL = 'http://122.165.117.252:8701/api/user';
const productionURL = 'http://94-237-73-65.sg-sin1.upcloud.host:96/api/';

const baseURL = productionURL;
const Server = axios.create({
  baseURL: baseURL,
});
///SIGN IN SERVICE
export const SignIn = loginData => {
  var dataToSend = {
    email: loginData.username,
    password: loginData.password,
  };
  console.log('login data' + loginData.username + loginData.password);
  return async dispatch => {
    try {
      dispatch({
        type: 'ShowLoader',
        payload: true,
      });
      console.log('signin service called');
      const response = await Server.post('Auth/user_Login', dataToSend);
      console.log(response.data);

      let dataObject = response.data;
      // console.log(dataObject);
      console.log(dataObject.data);

      if (dataObject.status === 1) {
     
//await AsyncStorage.setItem('UserId', dataObject.data.id);
        //dispatch({type: 'add_Code', payload: acessCode});

        console.log(dataObject.data.fullname);

        console.log('signin service success');
        dispatch({
          type: 'SignInSucess',
          payload: dataObject,
        });
        navigate('Dashboard');
      } else {
        dispatch({
          type: 'SignInFailed',
          payload: dataObject.message,
        });
      }
    } catch (err) {
      console.log(err);

      dispatch({
        type: 'SignInFailed',
        payload: err.message,
      });
    }
  };
};
