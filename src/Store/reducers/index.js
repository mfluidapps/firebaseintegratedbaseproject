import loginReducer from './LoginReducer';
import {combineReducers} from 'redux';
export default combineReducers({
  SignIn: loginReducer
});
