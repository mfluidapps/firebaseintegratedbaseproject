import React, {Component, useContext, useEffect} from 'react';
import {
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  FlatList,
  Text,
  TouchableOpacity,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  Modal,
  WebView,
} from 'react-native';

import {connect} from 'react-redux';
import * as actions from '../../Store/actions'
import {DynamicHeader} from '../../components/DynamicHeader';
import Icon from 'react-native-vector-icons/AntDesign';
import CardView from 'react-native-rn-cardview';
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
import LinearGradient from 'react-native-linear-gradient';
import TouchableScale from 'react-native-touchable-scale';
import {Overlay, Button} from 'react-native-elements';
import axios from 'axios';

import {auth} from 'react-native-firebase';

class Dashboard extends Component {
  constructor() {
    super();
    this.axios = axios;
    this.state = {
      dashboardItems: [
        {
          title: 'New Appointment',
          index: 1,
          color: '#e74c3c',
          textColor: 'white',
          bottomLineColor: '#e74c3c',
        },
        {
          title: 'Booking History',
          index: 2,
          color: '#27ae60',
          textColor: 'white',
          bottomLineColor: '#27ae60',
        },
        {
          title: 'Dependent Circle',
          index: 3,
          color: '#ecf0f1',
          textColor: 'gray',
          bottomLineColor: '#cccccc',
        },
        {
          title: 'My Documents',
          index: 4,
          color: '#ecf0f1',
          textColor: 'gray',
          bottomLineColor: '#cccccc',
        },
      ],
      userName: '',
      overLay: false,
    };
    //BookService
  }
  UNSAFE_componentWillMount() {
    // const name = AsyncStorage.getItem('username');
    // if (name !== null) {
    //   // We have data!!
    //   console.log(name);
    //   //this.setState({userName:name})
    // }
    //this.props.DependentSignUp();
    this._retrieveData();
   // this.props.GetUserInfo();
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('username');
      if (value !== null) {
        // We have data!!
        console.log('username is');
        console.log(value);
        this.setState({userName: value});
      }
    } catch (error) {
      // Error retrieving data
    }
  };
  logoutAction = () => {
    this.setState({overLay: false});
    this.props.LogOut();
    //this.props.navigation.navigate('LoginScreen');
  };

  render() {
    return (
      <>
        <StatusBar
          barStyle="light-content"
          backgroundColor="#2edccd"
          translucent={false}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: '#2edccd'}} />
        <SafeAreaView style={styles.container}>
          <View style={{backgroundColor: 'white', flex: 1}}>
            <LinearGradient
              colors={['#2edccd', '#58d3e1', '#76ccef']}
              style={styles.linearGradient}>
              <View style={{alignItems: 'center', height: 200}}>
                <View
                  style={{
                    flexDirection: 'row',
                    top: 20,
                    width: deviceWidth,
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      //this.props.navigation.navigate('LoginScreen');
                      this.setState({overLay: true});
                    }}>
                    <View>
                      <Icon
                        style={{marginLeft: 15}}
                        name="logout"
                        size={25}
                        color="white"
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('Profile');
                    }}>
                    <View>
                      <Icon
                        style={{marginRight: 10}}
                        name="user"
                        size={28}
                        color="white"
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <Text
                  style={{
                    marginTop: 100,
                    fontWeight: '300',
                    color: 'white',
                    fontSize: 29,
                  }}>
                  {this.state.userName}
                </Text>
              </View>
            </LinearGradient>

            <FlatList
              style={{paddingHorizontal: 10, paddingTop: 10}}
              data={this.state.dashboardItems}
              keyExtractor={object => {
                return object.index;
              }}
              columnWrapperStyle={styles.row}
              numColumns={2}
              scrollEnabled={false}
              renderItem={({item, index}) => {
                return (
                  // <TouchableOpacity
                  <TouchableScale
                    friction={90} //
                    tension={100} // These props are passed to the parent component (here TouchableScale)
                    activeScale={0.94} //
                    onPress={() => {
                      switch (index) {
                        case 0:
                          // this.props.navigation.navigate('BookService');
                          this.props.navigation.navigate('NewAppoinment');

                          break;
                        case 1:
                          this.props.navigation.navigate('ConsultationHistory');

                          break;
                        case 2:
                          this.props.navigation.navigate('DependentList');

                          break;

                        case 3:
                          this.props.navigation.navigate('MyDocuments');
                          // this.props.navigation.navigate('PaymentHistory');
                          //this.props.navigation.navigate('Paypal');

                          break;

                        default:
                          break;
                      }
                    }}>
                    <View style={styles.listItemtwo}>
                      <CardView
                        cardElevation={1}
                        maxCardElevation={1}
                        radius={0}
                        backgroundColor={item.color}>
                        <View
                          style={{
                            padding: 10,
                            height: 120,
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              paddingTop: 42,
                              fontWeight: '300',
                              color: item.textColor,
                              fontSize: 19,
                            }}>
                            {item.title}
                          </Text>
                        </View>
                        <View
                          style={{
                            backgroundColor: item.bottomLineColor,
                            height: 4,
                          }}
                        />
                      </CardView>
                    </View>

                    {/* </TouchableOpacity> */}
                  </TouchableScale>
                );
              }}
            />
          </View>
          <Overlay isVisible={this.state.overLay}>
            <View style={styles.overlay}>
              <Text
                allowFontScaling={false}
                style={{fontSize: 18, fontWeight: '700'}}>
                Warning!
              </Text>
              <Text
                allowFontScaling={false}
                style={{fontSize: 14, fontWeight: '400', marginVertical: 20}}>
                Do you want to logout?
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  // backgroundColor: 'gray',
                  width: Dimensions.get('window').width - 80,
                  justifyContent: 'space-between',
                  position: 'absolute',
                  bottom: 0,
                }}>
                <Button
                  title="Logout"
                  type="outline"
                  containerStyle={{flex: 1}}
                  onPress={() => this.logoutAction()}
                  titleStyle={{fontSize: 15}}
                />
                <Button
                  title="Cancel"
                  type="outline"
                  containerStyle={{flex: 1}}
                  onPress={() => this.setState({overLay: false})}
                  titleStyle={{fontSize: 15}}
                />
              </View>
            </View>
          </Overlay>
        </SafeAreaView>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  overlay: {
    height: Dimensions.get('window').height / 4 - 50,
    width: Dimensions.get('window').width - 80,
    alignItems: 'center',
  },
  listItem: {
    flex: 1,
    height: 50,
    borderWidth: 0.2,
    marginHorizontal: 45,
    backgroundColor: 'white',
    alignItems: 'center',
    marginTop: 20,
    borderRadius: 8,
  },
  row: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  listItemtwo: {
    width: (deviceWidth - 55) / 2,
    maxWidth: deviceWidth / 2,
    backgroundColor: '#fff',
    marginBottom: 10,
    borderRadius: 0,
    marginTop: 10,
    marginHorizontal: 10,
  },
});
Dashboard.navigationOptions = {
  headerShown: false,
};
//export default Dashboard;
const mapStateToProps = state => {
  //console.log(state);
  return {State: state.SignIn};
};
export default connect(
  mapStateToProps,
  actions,
)(Dashboard);
