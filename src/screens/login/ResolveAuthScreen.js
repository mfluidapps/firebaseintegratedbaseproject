import React, {useEffect} from 'react';
import {AsyncStorage, ImageBackground, SafeAreaView} from 'react-native';
const ResolveAuthScreen = ({navigation}) => {
  setUserTypeValues=async()=>{
    await AsyncStorage.setItem('UserType', "MySelf");
    await AsyncStorage.setItem('DependentFK',"");

  }
  checkIsSignedIn = async () => {
    this.setUserTypeValues();
    const userLoggedIn = await AsyncStorage.getItem('UserId');
    if (!userLoggedIn) {
      navigation.navigate('LoginFlow');
    } else {
      navigation.navigate('MainFlow');
    }
  };
  useEffect(() => {
    checkIsSignedIn();
  }, []);
  return (
    <SafeAreaView style={{flex: 1}}>
      {/* <ImageBackground style={{ flex: 1 }} source={require('../../../assets/LaunchBackgrnd.png')} /> */}
    </SafeAreaView>
  );
};

export default ResolveAuthScreen;
