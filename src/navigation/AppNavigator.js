import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoginScreen from '../screens/login/LoginScreen';
import Dashboard from '../screens/dashboard/Dashboard';
import ResolveAuthScreen from '../screens/login/ResolveAuthScreen';

const loginFlow = createStackNavigator(
    {
      LoginScreen: {screen: LoginScreen}
    },
    {
      initialRouteName: 'LoginScreen',
    },
  );


  const mainFlow = createStackNavigator(
    {
      Dashboard: {screen: Dashboard}
    },
    {
      //initialRouteName: 'DependencyCircle',
      defaultNavigationOptions: {header: null},
    },
  );

  const App = createSwitchNavigator({
    ResolveScreen: ResolveAuthScreen,
    LoginFlow: loginFlow,
    MainFlow: mainFlow,
  });
  
  export default createAppContainer(App);